#include <TimerOne.h>
#include <MIDI.h>

/////////////////////////// CONSTANTES //////////////////////
const int led = LED_BUILTIN;
const int PotVelocite = A8;
const int NoteOnButton = 14;
const int valeur_BPM = A9;
const byte CLK_DIVIDER_MAX = 24;
const byte ClkDividerSetup[8] = {1, 2, 3, 4, 6, 8, 12, 24};

/////////////////////////// VARIABLES GLOBALES //////////////////////
MIDI_CREATE_INSTANCE(HardwareSerial, Serial1, MIDI);
volatile byte velocite[CLK_DIVIDER_MAX] = {127, 0, 0, 0, 0, 0, 20, 0, 0, 0, 0, 0, 40, 0, 0, 0, 0, 0, 60, 0, 0, 0, 0, 0};
volatile byte PosSeq = -1;
unsigned long tps;

/////////////////////////// FONCTIONS //////////////////////
/**
  * @exception  Incremente l'horloge interne du séquenceur. Met à jour le tableau de vélocité de la note joué si nécessaire.
  */
void send_MIDI(void)
{
  PosSeq++;
  if(PosSeq > 23) PosSeq = 0;

  // Si l'utilisateur appuie sur le bouton et que la position courante dans la séquence = la division temporelle choisi
  // on met à jour le tableau de vélocité avec la nouvelle valeur.
  if(!(digitalRead(NoteOnButton)) && PosSeq%(CLK_DIVIDER_MAX/ClkDividerSetup[0])==0)
  {
    byte velo;
    velo = byte(map(analogRead(PotVelocite), 1, 1024, 0, 127));
    Serial.println(velo);
    velocite[PosSeq] = velo;
  }
}

/**
  * @fn int conv_BPM_us(int _BPM)
  * @brief Convertit la valeur du nombre de BPM (Battements par minute) en une période en us.
  * @param _BPM: le nombre de battements par minute.
  */
int conv_BPM_us(int _BPM)
{
  float frequence_interruption;
  float periode_interruption_us;
  frequence_interruption = float(float(_BPM)*float(24)/float(60));
  periode_interruption_us = 1.0/frequence_interruption*1000000;
  return int(periode_interruption_us);
  
}

/**
  * @fn void gestion_BPM()
  * @brief Gère l'acquisition du nombre de BPM, sa conversion en microsecondes et la mise à jour de l'horloge du séquenceur si nécessaire.
  */
void gestion_BPM()
{
  int BPM;
  int BPM_us;
  static int last_BPM = 120;

  BPM = analogRead(valeur_BPM);  
  BPM = map(BPM, 1, 1024, 20, 180);
  
  if(BPM != last_BPM)
  {
    Serial.println("BPM = ");
    Serial.println(BPM);
    BPM_us = conv_BPM_us(BPM);
    Timer1.setPeriod(BPM_us);
    last_BPM = BPM;
  }
}

/**
  * @fn void PlaySequence()
  * @brief Envoie les notes Midi correspondant à la position actuelle dans la séquence.
  */
void PlaySequence()
{
  static byte LastPosConnue = -1;
  static int ledState = LOW;
  if(PosSeq != LastPosConnue)
  {
    // Clignotage led
    ledState = !ledState;
    digitalWrite(led, ledState);
    // Clignotage led
    //Serial.println("Position dans la séquence");
    //Serial.println(PosSeq);
  
    MIDI.sendNoteOn(64, velocite[PosSeq], 3);
    LastPosConnue = PosSeq;
  }  
}

/////////////////////////// MAIN //////////////////////
void setup(void)
{
  MIDI.begin(MIDI_CHANNEL_OMNI);
  pinMode(led, OUTPUT);
  pinMode(NoteOnButton, INPUT_PULLUP);
  Serial.begin(9600);
  Timer1.initialize(50000);
  Timer1.attachInterrupt(send_MIDI);
}

void loop(void)
{
  if(millis() - tps > 200)
  {
    gestion_BPM();
    tps = millis();
  }
  PlaySequence();
}
